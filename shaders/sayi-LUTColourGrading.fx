#include "ReShade.fxh"
#include "ReShadeUI.fxh"

#ifndef Sayi_LUT_TextureName
    #define Sayi_LUT_TextureName "neutral.png"
#endif
#ifndef Sayi_LUT_TileSize
    #define Sayi_LUT_TileSize 32.0
#endif
#ifndef Sayi_LUT_AmountTiles
    #define Sayi_LUT_AmountTiles 32.0
#endif

texture LUTtexture <
    source = Sayi_LUT_TextureName;
> {
    Width = Sayi_LUT_TileSize * Sayi_LUT_AmountTiles;
    Height = Sayi_LUT_TileSize;
    Format = RGBA8;
};
sampler LUTSampler { Texture = LUTtexture; };

static const float MaxColour = 31.0;
static const float Width = Sayi_LUT_TileSize * Sayi_LUT_AmountTiles;
// 32 bit colour in RGBA8
static const float ColourDepth = 32.0;
static const float Threshold = MaxColour / ColourDepth;

float3 LUTPass(float4 vpos : SV_Position, float2 texcoord : TexCoord) : SV_Target
{
    float3 original = tex2D(ReShade::BackBuffer, texcoord).rgb;

    float halfX = 0.5 / Width;
    float halfY = 0.5 / Sayi_LUT_TileSize;

    float offsetX = halfX + original.r * Threshold / ColourDepth;
    float offsetY = halfY + original.g * Threshold;
    float cell = floor(original.b * MaxColour);
    float2 lutUV = float2(cell / ColourDepth + offsetX, offsetY);
    return tex2D(LUTSampler, lutUV).rgb;
}

technique SayiLUTColourGrading
{
    pass
    {
        VertexShader = PostProcessVS;
        PixelShader = LUTPass;
    }
}