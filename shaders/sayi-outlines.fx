#include "ReShade.fxh"
#include "ReShadeUI.fxh"

uniform float3 LineColour <
    ui_label = "Colour";
    ui_type = "color";
> = float3(0.0, 0.0, 0.0);

uniform float Thiccness <
    ui_label = "Thiccness";
    ui_type = "slider";
    ui_min = 0.0;
    ui_max = 2.0;
> = 1.0;

uniform float Threshold <
    ui_label = "Threshold";
    ui_type = "slider";
    ui_min = 0.0;
    ui_max = 20;
> = 0.1;

uniform float Opacity <
    ui_label = "Opacity";
    ui_type = "slider";
    ui_min = 0.0;
    ui_max = 1.0;
> = 1.0;

uniform bool DisableOnKeyDown <
    ui_label = "Disable on G Key Down";
> = false;

uniform bool GKeyDown <
    source = "key";
    keycode = 71;
>;

float max4(float4 values)
{
    return max(values.x, max(values.y, max(values.z, values.w)));
}

float min4(float4 values)
{
    return min(values.x, min(values.y, min(values.z, values.w)));
}

float3 OutlinesPass(float4 vpos : SV_Position, float2 texcoord : TexCoord) : SV_Target
{
    float3 original = tex2D(ReShade::BackBuffer, texcoord).rgb;

    [branch] if(DisableOnKeyDown)
    {
        if(GKeyDown)
        {
            return original;
        }
    }

    float4 texelSize = float4(Thiccness / BUFFER_WIDTH, Thiccness / BUFFER_HEIGHT, -(Thiccness / BUFFER_WIDTH), -(Thiccness / BUFFER_HEIGHT));
    float currPixelDistance = ReShade::GetLinearizedDepth(texcoord);

    // horizontal/vertical depths
    float4 depthHV = float4(
        ReShade::GetLinearizedDepth(texcoord + float2(0.0, texelSize.w)),
        ReShade::GetLinearizedDepth(texcoord + float2(0.0, texelSize.y)),
        ReShade::GetLinearizedDepth(texcoord + float2(texelSize.x, 0.0)),
        ReShade::GetLinearizedDepth(texcoord + float2(texelSize.z, 0.0))
    );

    // diagonal depths
    float4 depthDiagonal = float4(
        ReShade::GetLinearizedDepth(texcoord + float2(texelSize.x, texelSize.w)),
        ReShade::GetLinearizedDepth(texcoord + float2(texelSize.z, texelSize.y)),
        ReShade::GetLinearizedDepth(texcoord + float2(texelSize.x, texelSize.y)),
        ReShade::GetLinearizedDepth(texcoord + float2(texelSize.z, texelSize.w))
    );

    // normalisation
    float2 minDistance = float2(min4(depthHV), min4(depthDiagonal));
    float2 maxDistance = float2(max4(depthHV), max4(depthDiagonal));
    float magnitude = max(maxDistance.x, maxDistance.y) - min(minDistance.x, minDistance.y) + Threshold / 100;
    currPixelDistance /= magnitude;
    depthHV /= magnitude;
    depthDiagonal /= magnitude;

    // getting to the edge
    float4 differenceHV = abs(depthHV - currPixelDistance);
    float4 differenceDiagonal = abs(depthDiagonal - currPixelDistance);

    float2 edge = float2(
        max(abs(differenceHV.x - differenceHV.y), abs(differenceHV.z - differenceHV.w)),
        max(abs(differenceDiagonal.x - differenceDiagonal.y), abs(differenceDiagonal.z - differenceDiagonal.w))
    );

    float maxEdgeLevel = max(edge.x, edge.y);
    return lerp(original, LineColour, maxEdgeLevel * Opacity);
}

technique SayiOutline
{
    pass
    {
        VertexShader = PostProcessVS;
        PixelShader = OutlinesPass;
    }
}