#include "ReShade.fxh"
#include "ReShadeUI.fxh"
#include "sayi-utils.fx"

#define CATEGORY_NIGHT_VISION "Night Vision"

uniform bool NightVisionActive <
    ui_category = CATEGORY_NIGHT_VISION;
    ui_label = "Night Vision Active";
> = false;

uniform float3 NVColour <
    ui_category = CATEGORY_NIGHT_VISION;
    ui_label = "Colour";
    ui_type = "color";
> = float3(0.0, 1.0, 0.0);

uniform float NVIntensity <
	ui_category = CATEGORY_NIGHT_VISION;
	ui_label = "Night Vision Strength";
	ui_type = "slider";
	ui_min = 1.0;
	ui_max = 20.0;
> = 10.0;

uniform float NVLumaMidpoint <
	ui_category = CATEGORY_NIGHT_VISION;
	ui_label = "Luminance Midpoint";
	ui_type = "slider";
	ui_min = 0.0;
	ui_max = 1.0;
> = 0.1;

uniform float NVLumaIntensity <
	ui_category = CATEGORY_NIGHT_VISION;
	ui_label = "Luminance Intensity";
	ui_type = "slider";
	ui_min = 1.0;
	ui_max = 10.0;
> = 5;

float3 NightVisionPass(float4 vpos : SV_Position, float2 texcoord : TexCoord) : SV_Target
{
    float3 result = tex2D(ReShade::BackBuffer, texcoord).rgb;

    [branch] if(NightVisionActive)
    {
        float luminance = Luma(result);
        // boost luminance
        luminance = saturate(lerp(NVLumaMidpoint, luminance, NVLumaIntensity));
        result = NVColour * luminance * NVIntensity;
    }

    return result;
}

technique SayiNightVision
{
    pass
    {
        VertexShader = PostProcessVS;
        PixelShader = NightVisionPass;
    }
}