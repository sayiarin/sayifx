#include "ReShade.fxh"
#include "ReShadeUI.fxh"
#include "sayi-utils.fx"

#define CATEGORY_HSV "Colour Correction / HSV"
#define CATEGORY_RGB "RGB Value Multipliers"
#define CATEGORY_OTHER "Other Effects"

/** HSV Variables **/
uniform float Brightness <
    ui_category = CATEGORY_HSV;
    ui_type = "drag";
    ui_min = 0.0;
> = 1.0;

uniform float Hue <
    ui_category = CATEGORY_HSV;
    ui_type = "slider";
    ui_min = 0.0;
    ui_max = 360.0;
> = 0.0;

uniform float Saturation <
    ui_category = CATEGORY_HSV;
    ui_type = "drag";
    ui_min = 0.0;
> = 1.0;

uniform float Value <
    ui_category = CATEGORY_HSV;
    ui_type = "drag";
    ui_min = 0.0;
> = 1.0;

/** RGB Variables **/
uniform float RedChannel <
    ui_category = CATEGORY_RGB;
    ui_label = "Red Channel";
    ui_type = "drag";
    ui_min = 0.0;
> = 1.0;

uniform float GreenChannel <
    ui_category = CATEGORY_RGB;
    ui_label = "Green Channel";
    ui_type = "drag";
    ui_min = 0.0;
> = 1.0;

uniform float BlueChannel <
    ui_category = CATEGORY_RGB;
    ui_label = "Blue Channel";
    ui_type = "drag";
    ui_min = 0.0;
> = 1.0;

/** Other **/
uniform float Contrast <
	ui_category = CATEGORY_OTHER;
	ui_label = "Contrast";
	ui_type = "slider";
	ui_min = 0.0;
	ui_max = 2.0;
> = 1.0;

uniform bool InvertColours <
    ui_category = CATEGORY_OTHER;
    ui_label = "Invert Colours";
> = false;

uniform bool FunnyEffect <
    ui_category = CATEGORY_OTHER;
    ui_label = "Funny Effect";
> = false;

uniform float FunModulo <
    ui_category = CATEGORY_OTHER;
    ui_label = "Modulo for Funny Effect";
    ui_type = "slider";
    ui_min = 0.0001f;
    ui_max = 0.99f;
> = 0.1;

float3 ApplyContrast(float3 colour, float contrast)
{
    return (colour.rgb - 0.5) * contrast + 0.5;
}

float3 SayiColourFXPass(float4 vpos: SV_Position, float2 texcoord: TexCoord) : SV_Target
{
    float3 colour = saturate(tex2D(ReShade::BackBuffer, texcoord).rgb);

    // rgb channel adjustments
    colour.r *= RedChannel;
    colour.g *= GreenChannel;
    colour.b *= BlueChannel;

    // colour inversion
    [branch] if(InvertColours)
    {
        colour = 1 - colour;
    }

    // thanks aero
    [branch] if(FunnyEffect)
    {
        colour = colour % FunModulo;
    }

    // simple Brightness
    colour *= Brightness;

    // HSV changes
    colour = ApplyHSVChangesToRGB(colour, float3(Hue, Saturation, Value));
    
    // apply contrast
    colour = ApplyContrast(colour, Contrast);

    return colour;
}

technique SayiColourFX
{
    pass
    {
        VertexShader = PostProcessVS;
        PixelShader = SayiColourFXPass;
    }
}