#include "ReShade.fxh"
#include "ReShadeUI.fxh"
#include "sayi-utils.fx"

#define CATEGORY_HSV "Colour Correction / HSV"

uniform float3 OriginalColour <
    ui_label = "Original Colour";
    ui_type = "color";
> = float3(1.0, 0.5, 0);

uniform float3 ReplacementColour <
    ui_label = "Replacement Colour";
    ui_type = "color";
> = float3(0.0, 1.0, 1.0);

uniform int ReplacementMethod <
    ui_label = "Replacement Method";
    ui_type = "combo";
    ui_items = 
    "Flat Colour\0"
    "Shaded\0"
    "Hue Shift\0";
> = 1;

uniform float Hue <
    ui_category = CATEGORY_HSV;
    ui_type = "slider";
    ui_min = 0.0;
    ui_max = 360.0;
> = 0.0;

uniform float Saturation <
    ui_category = CATEGORY_HSV;
    ui_type = "drag";
    ui_min = 0.0;
> = 1.0;

uniform float Value <
    ui_category = CATEGORY_HSV;
    ui_type = "drag";
    ui_min = 0.0;
> = 1.0;

uniform float Tolerance <
    ui_label = "Tolerance";
    ui_type = "slider";
    ui_min = 0.0;
    ui_max = 1;
> = 1;

uniform bool OnKeyDownOnly <
    ui_label = "On E Key Down Only";
> = true;

uniform bool EKeyDown <
    source = "key";
    keycode = 69;
    // nice
>;

float3 GetCorrectedColour(float3 original)
{
    if(distance(normalize(original), OriginalColour) <= Tolerance)
    {
        [branch] if(ReplacementMethod == 0)
        {
            // flat colour
            return ReplacementColour;
        } 
        else if (ReplacementMethod == 1)
        {
            // Shaded (using luma)
            return Luma(original) * ReplacementColour;
        }
        else if (ReplacementMethod == 2)
        {
            // Hue Shift
            return ApplyHSVChangesToRGB(original, float3(Hue, Saturation, Value));
        }
    }
    return original;
}

float3 SayiSingleColourOverride(float4 vpos : SV_Position, float2 texcoord : TexCoord) : SV_Target
{
    float3 colour = tex2D(ReShade::BackBuffer, texcoord).rgb;

    [branch] if(OnKeyDownOnly)
    {
        if(EKeyDown)
        {
            colour = GetCorrectedColour(colour);
        }
    }
    else
    {
        colour = GetCorrectedColour(colour);
    }

    return colour;
}

technique SayiSingleColourOverride
{
    pass{
        VertexShader = PostProcessVS;
        PixelShader = SayiSingleColourOverride;
    }
}