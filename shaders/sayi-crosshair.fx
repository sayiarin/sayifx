#include "ReShade.fxh"
#include "ReShadeUI.fxh"

#define CATEGORY_GENERAL "General"

uniform int OffsetX <
    ui_category = CATEGORY_GENERAL;
    ui_label = "X Axis Offset";
    ui_type = "drag";
    ui_min = -(BUFFER_WIDTH / 2);
    ui_max = BUFFER_WIDTH / 2;
> = 0;

uniform int OffsetY <
    ui_category = CATEGORY_GENERAL;
    ui_label = "X Axis Offset";
    ui_type = "drag";
    ui_min = -(BUFFER_HEIGHT / 2);
    ui_max = BUFFER_HEIGHT / 2;
> = 0;

uniform float3 CrosshairColour <
    ui_category = CATEGORY_GENERAL;
    ui_label = "Colour";
    ui_type = "color";
> = float3(1.0, 1.0, 1.0);

uniform float Opacity <
    ui_category = CATEGORY_GENERAL;
    ui_label = "Opacity";
    ui_type = "slider";
    ui_min = 0.0;
    ui_max = 1.0;
> = 1.0;

uniform float Thiccness <
	ui_category = CATEGORY_GENERAL;
	ui_label = "Thiccness";
	ui_type = "slider";
	ui_min = 0.0;
	ui_max = 20.0;
> = 2.0;

uniform float Radius <
	ui_category = CATEGORY_GENERAL;
	ui_label = "Radius";
	ui_type = "slider";
	ui_min = 0.0;
	ui_max = BUFFER_HEIGHT / 8;
> = 4.0;

// able to hide crosshair when holding right mouse and/or shift
uniform int HideSetting <
	ui_category = CATEGORY_GENERAL;
	ui_label = "Hide Settings";
	ui_type = "combo";
	ui_items =
	"Never Hide\0"
	"On Right Mouse Button\0"
	"On Shift Button\0"
	"On Both Pressed\0";
> = 0;

uniform bool RightMouseDown <
	source = "mousebutton";
	keycode = 1;
>;

uniform bool ShiftDown <
	source = "key";
	keycode = 16;
>;

float3 SayiCrosshairPass(float4 vpos: SV_Position, float2 texcoord : TexCoord) : SV_Target
{
    float3 original = tex2D(ReShade::BackBuffer, texcoord).rgb;
    
    // Never Hide
    [branch] if(HideSetting == 0)
    {
		// do nothing
	}
    
    // on right mouse
    [branch] if(HideSetting == 1)
    {
		if(RightMouseDown) return original;
	}

	// on shift
	[branch] if(HideSetting == 2)
    {
		if(ShiftDown) return original;
	}

	// on both
	[branch] if(HideSetting == 3)
    {
		if(RightMouseDown && ShiftDown) return original;		
	}
    
    float2 center = float2((BUFFER_WIDTH / 2) + OffsetX, (BUFFER_HEIGHT / 2) + OffsetY);
	float distCenter = distance(center, vpos.xy);	
	float3 crosshair = lerp(original, CrosshairColour, saturate(max(Thiccness - abs(distCenter - (Radius + Thiccness / 2.0)), 0)));
    return lerp(original, crosshair, Opacity);
}

technique SayiCrosshair
{
    pass
    {
        VertexShader = PostProcessVS;
        PixelShader = SayiCrosshairPass;
    }
}