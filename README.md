# SayiFX

My attempts at creating post-processing effect shaders for [crosires reshade](https://github.com/crosire/reshade).

The files are prefixed with `sayi-` because that makes it easier for me to find them in the list of available shaders.

## Colour
A simple colour correction shader that allows you to use sliders to easily adjust the games overall brightness, saturation, luminance and contrast. There are also sliders for adjusting the individual rgb colour channels.

On top of that there are two less useful but more fun features:
* a hue shift slider to change the colour hue of the end result
* a colour inversion mode

## Crosshair
Draws a circle at the center of the screen.

It can easily adjusted with sliders for offsets on both axis, opacity and size with sliders.
There is also a colour picker to change the colour of the crosshair.

## Night Vision
A simple "Night Vision" shader that calculates the luminance of each pixel and boosts it by a given intensity if it is below an adjustable threshold.
This results in a gray-scale output that can be colour graded.

## Single Colour Override
A very simple override for a specific colour. Adjustable with a Threshold. It can be toggled to be either always active or only when the `E` key is being pressed down.
It comes in three different modes:
* **Flat Colour** - Replaces a specified Colour with another one without any shading
* **Shaded** - Uses Luma Value to somehwat keep shading intact
* **HSV** - Uses HSV settings to change the specified colour 

I made this mainly for the game Hunt: Showdown. There once were colour grading options in the game engines settings that would change the colours seen in 'Dark Sight' from orange to cyan which I liked a lot. This is a very naïve implementation to bring that back for myself.

## Outlines
A shader that accesses the Depth Buffer to calculate the distance of the current pixel and all it's neighbours to detect whether or not there is an edge. If an edge is detected, the pixel(s) are changed to a configurable solid colour.
It has some very basic settings for a Threshold (distance to camera), Thickness, Line Colour and Opacity.

I usually rebind my overlay map to the `G` key, so there is an option to disable the outlines as long as this button is pressed since reshade is a post processing effect which may render on top of UI Elements.

## LUT Colour Grading
Allows to use a LUT texture for colour grading. I still have to figure out a good UI for it and maybe include some example LUTs.
